//Callback Hell Version

const fs = require('fs');

fs.readFile('body.txt', 'utf8', (err, data) => {
    let accumData = data + '\r\n';
    fs.readFile('feet.txt', 'utf8', (err, data) => {
        accumData += data + '\r\n';
        fs.readFile('head.txt', 'utf8', (err, data) => {
            accumData += data + '\r\n';
            fs.readFile('leg.txt', 'utf8', (err, data) => {
                accumData += data + '\r\n';
                fs.writeFile('robot.txt', accumData, 'utf8', (err, data) => {
                    if (err) throw err;
                    console.log("Write robot.txt file completed");
                    fs.readFile('robot.txt', 'utf8', (err, data) => {
                        console.log(data);
                    })
                })
            })
        })
    })
})