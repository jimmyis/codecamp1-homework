const fs = require('fs');

fs.readFile('employee.txt', 'utf8', (err, data) => {
    let json = JSON.parse(data);
    for (index in json) {
        let personName = '';
        for (key in json[index]) {
    
            if (key == 'firstname') {
                personName += json[index][key];
            }
    
            if (key == 'lastname') {
                personName += ' ' + json[index][key];
            }
        
        }
        console.log(personName);
    }
})