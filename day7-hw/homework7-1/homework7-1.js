// Codecamp Thailand #1 Day 7 Homework 1

const fs = require('fs');

fs.readFile('homework1.json', 'utf8', (err, json) => {
    let employees = JSON.parse(json);
    let employeeDummy = [
        { name: 'Darth Vader', salary: 'Light Sabre' }
    ];

    employees.map(profile => {
        addAdditionalFields(profile);
    })
})

function addYearSalary (data) {
    const error = {
        noinput: 'No input data'
    }

    if (data !== undefined) {
        if (data.hasOwnProperty('salary')) {
            data.yearSalary = data.salary * 12;
            return data.yearSalary;
        }
    } else {
        console.log(`Error: ${error.noinput}`);
        return { error: error.noinput };
    }
}

function addNextSalary (data) {
    const error = {
        noinput: 'No input data',
        nofield: 'No salary field in input data',
        invalidnumber: 'Salary field is not number'
    }
    let nextSalary = [];
    if (data !== undefined) {
        if (data.hasOwnProperty('salary')) {
            if (typeof data.salary == 'number') {
                nextSalary[0] = data.salary;
                nextSalary[1] = parseInt(nextSalary[0] * 1.1);
                nextSalary[2] = parseInt(nextSalary[1] * 1.1);
                return nextSalary;
            } else  {
                console.log(`Error: ${error.invalidnumber}`);
                return { error: error.invalidnumber };
            }
        } else {
            console.log(`Error: ${error.nofield}`);
            return { error: error.nofield };
        }
    } else {
        console.log(`Error: ${error.noinput}`);
        return { error: error.noinput };
    }
}

function addAdditionalFields (profile) {
    profile.yearSalary = addYearSalary(profile);
    profile.nextSalary = addNextSalary(profile);
    console.log(profile)
}

