let arr = [1,2,3,4,5,6,7,8,9,10];

const evenNumber = (arr) => { 
    let result = arr.filter(number => {
        return number % 2 == 0
    })
    return result
}

const x1000 = (arr) => {
    let result = arr.map(number => {
        return number * 1000
    })
    return result
}

console.log(x1000(evenNumber(arr)))