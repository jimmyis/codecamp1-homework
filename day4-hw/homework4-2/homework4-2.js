const fs = require('fs');
const basepath = __dirname + '/';
const sourcefile = basepath + 'homework1.json';

// Join firstname and lastname with space inbetween
const makeFullname = (firstname, lastname) => {
    return firstname + ' ' + lastname;
}

// Add fullname to profile
const addFullname = (profile) => { 
    profile['fullname'] =  makeFullname(profile['firstname'], profile['lastname'])
}

// Increase salary by rate
const increaseSalary = (salary, rate) => {
    return parseInt(salary * rate);
}

// Project salary forward by years
const projectSalary = (profile, years) => {
    profile['salary'] = [profile['salary']]
    for (let i = 1; i < years; i++) {
        let baseSalary = profile['salary'][i-1];
        profile['salary'][i] = increaseSalary(baseSalary, 1.1);

    }
    return profile;
}

// Merge between old and new profile, and return updated profile
const updateProfile = (oldProfile, newProfile, key) => {
    let updatedProfile;
    // Match profile key

}

fs.readFile(sourcefile, 'utf8', (err, done) => {
    if (err) console.log(err);
    else {
        let profiles = JSON.parse(done);
        
        profiles.map((profile) => {
            return profile = addFullname(profile);
        })
        
        profiles.map((profile) => {
            return profile = projectSalary(profile, 3);
        })

        console.log(profiles)
    }
})