const fs = require('fs');
const basepath = __dirname + '/';
const sourcefile = basepath + 'homework1.json';

const eachProfile = (profiles) => {
    return profiles.map((profile) => {
        return profile
    })
}

// Double salary
const doubleSalary = (profile) => {
    return profile['salary'] * 2
}

// Summarize payroll

fs.readFile(sourcefile, 'utf8', (err, done) => {
    if (err) console.log(err);
    else {
        let profiles = JSON.parse(done);

        let filtratedProfiles = profiles
        .filter((profile) => {
            return profile.salary < 100000
        }).map((profile) => {
            profile.salary = profile.salary * 2
            return profile
        })
        
        profiles[0] = filtratedProfiles[0];
        profiles[2] = filtratedProfiles[1];

        let sumPayroll = profiles
        .map(profile => {
            return profile['salary']  
        }).reduce((sum, salary) => {
            return sum + salary;
        })
        console.log("Payroll summary is " + sumPayroll)

    }
})