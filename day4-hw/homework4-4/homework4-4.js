const fs = require('fs');
const sourceData = __dirname + '/homework1-4.json';

fs.readFile(sourceData, 'utf8', (err, data) => {
    if (err) console.log(err);
    else {
        let profiles = JSON.parse(data);
        
        let newProfiles = profiles
            // Filter profiles that have more than 2 friends
            .filter((profile) => {
                return profile['friends'].length > 2;
            })
            // Filter remain profiles that gender is male
            .filter((profile) => {
                return profile['gender'] === 'male';
            })
            // Filter key from profiles 
              // get only these keys 'name', 'gender', 'company', 'email', 'friends', 'balance'
              // to reconstruct a new array
            // .filter((profile) => {
            //     Object.keys(profile).map((key) => {
            //         return key == 'name'
            //     })
            // })
            .map((profile) => {
                let balance = parseFloat(profile['balance'].replace(/\$|\,/g,''))
                
                balance = balance / 10;
                // Convert balance string to be floating point number
                    // Get rid of dollar sign

                let newProfile = {
                    name: profile.name,
                    gender: profile.gender,
                    company: profile.company,
                    email: profile.email,
                    friends: profile.friends,
                    balance: balance
                }
                return newProfile;
            })
        
        console.log(newProfiles);
    }
})