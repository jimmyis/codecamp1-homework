let peopleSalary = [
    {
        id: 1001,
        firstname: "Luke",
        lastname: "Skywalker",
        company: "Walt Disney",
        salary: 40000
    },
    {
        id: 1002,
        firstname: "Tony",
        lastname: "Stark",
        company: "Marvel",
        salary: 1000000
    },
    {
        id: 1003,
        firstname: "Somchai",
        lastname: "Jaidee",
        company: "Love2work",
        salary: 20000
    },
    {
        id: 1004,
        firstname: "Monkey D",
        lastname: "Luffee",
        company: "One Piece",
        salary: 9000000
    }
]

let newPeopleSalary = [];

for (const peopleByOrder in peopleSalary) {
    let people = peopleSalary[peopleByOrder];
    for (const key in people) {
        if (key == 'company') {
            delete people[key]
        }
        if (key == 'salary') {
            let salary = [people[key]]
            for( let i = 0; i < 2; i++ ) {
                let newSalary = salary[i]
                newSalary = newSalary + (salary[i] / 10);
                salary.push(newSalary)
            }
            people[key] = salary
        }
    }
    newPeopleSalary.push(people);
}

console.log(newPeopleSalary);
