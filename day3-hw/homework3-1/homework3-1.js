//Callback Hell Version

const fs = require('fs');

let readFile1 = new Promise((resolve, reject) => {
    fs.readFile('head.txt', 'utf8', (err, data) => {
        if (err) reject(err);
        else resolve(data);
    })
})

let readFile2 = new Promise((resolve, reject) => {
    fs.readFile('body.txt', 'utf8', (err, data) => {
        if (err) reject(err);
        else resolve(data);
    })
})

let readFile3 = new Promise((resolve, reject) => {
    fs.readFile('leg.txt', 'utf8', (err, data) => {
        if (err) reject(err);
        else resolve(data);
    })
})

let readFile4 = new Promise((resolve, reject) => {
    fs.readFile('feet.txt', 'utf8', (err, data) => {
        if (err) reject(err);
        else resolve(data);
    })
})

Promise.all([readFile1, readFile2, readFile3, readFile4])
.then(values => {
    let text = values.toString();
    text = text.replace(/,/gi, '\r\n');
    fs.writeFile('robot.txt', text, 'utf8', (err, data) => {
        if (err) throw err;
        console.log("Write robot.txt completed");
    })
})
.then(() => {
    fs.readFile('robot.txt', 'utf8', (err, data) => {
        console.log("Reading robot.txt")
        console.log(data);
    })
})
