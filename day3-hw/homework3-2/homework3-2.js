//Callback Hell Version

const fs = require('fs');

let readFile = (file) => {
    return new Promise((resolve, reject) => {
        fs.readFile(file, encode = 'utf8', (err, data) => {
            if (err) reject(err);
            else { 
                resolve(data);
            }
        })
    })
}

let writeFile = (file, arrayData, encode) => {
    return new Promise((resolve, reject) => {
        if (Array.isArray(arrayData)) {
            string = arrayData.toString();
            string = string.replace(/,/gi, '\r\n');
            fs.writeFile(file, string, encode = 'utf8', (err, data) => {
                if (err) throw err;
                console.log("Write robot.txt completed");
                resolve();
            })
        }
    })
}

async function createRobot() {
    try {
        let data = [] 
        data[0] = await readFile('head.txt');
        data[1] = await readFile('body.txt');
        data[2] = await readFile('leg.txt');
        data[3] = await readFile('feet.txt');
        await writeFile('robot.txt', data);
        let robot = await readFile('robot.txt');
        console.log(robot)
        return "done"
    } catch (error) {
        console.log("Eroororrororororororororororororrr!!!")
        console.log(error)
    }
}

createRobot()
.then((data) => {
    console.log("Done")
})