const fs = require('fs') 

let students =
[
    {"id":"1001",'firstname':'Luke','lastname':'Skywalker'},
    {"id":"1002",'firstname':'Tony','lastname':'Stark'},
    {"id":"1003",'firstname':'Somchai','lastname':'Jaidee'},
    {"id":"1004",'firstname':'Monkey D','lastname':'Luffee'},
];
let company = [
    {"id":"1001","company":"Walt Disney"},
    {"id":"1002","company":"Marvel"},
    {"id":"1003","company":"Love2work"},
    {"id":"1004","company":"One Piece"},
];
let salary = [
    {"id":"1001","salary":"40000"},
    {"id":"1002","salary":"1000000"},
    {"id":"1003","salary":"20000"},
    {"id":"1004","salary":"9000000"},
];
let like = [
    {"id":"1001","like":"apple"},
    {"id":"1002","like":"banana"},
    {"id":"1003","like":"orange"},
    {"id":"1004","like":"papaya"},
];
let dislike = [
    {"id":"1001","dislike":"banana"},
    {"id":"1002","dislike":"orange"},
    {"id":"1003","dislike":"papaya"},
    {"id":"1004","dislike":"apple"},
];

let employeesDatabase = [];
let fields = {
    company, salary, like, dislike
}
let peopleCount = students.length

for(let i = 0; i < peopleCount; i++){
    let eachPerson = students[i];
    let personID = eachPerson['id'];
    
    for (key in fields) {
        if (personID == fields[key][i]['id']) {
            eachPerson[key] = fields[key][i][key]
        }
    }
    employeesDatabase.push(eachPerson);
}

let json = JSON.stringify(employeesDatabase);

fs.writeFile('homework3-3.json', json, 'utf8', (err) => {
    if (err) console.log("Write file error. \r\n" + err);
    else console.log("Write file successfully.")
})
